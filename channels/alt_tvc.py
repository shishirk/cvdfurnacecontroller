import serial
from bidict import bidict

def funcmap(devname, devid):
    if devname=='tvc':
        t = tvc(devname, devid)
        c = 'init_state': t.init_state, 'open': t.valve_open, 'close': t.valve_close, 
            'get_pressure': t.get_pressure, 'get_position': t.get_position,
            'get_pressure_position': t.get_pressure_position,
            'get_setp_state': t.get_setp_state,
            'set_setp_state': t.set_setp_state, 
            'get_setp_params': t.get_setp_params,
            'set_setp_params': t.set_setp_params,
            'get_active_setp': t.get_actv_setp_ch,
            'activate_setp': t.activate_setp}
        return c, t.errcodes


import threading
# timeout = 0.025 is minimum to not give communication errors. Raising it to larger values
# increases the response times, for example at 0.1 s timeout, get_pressure_position is
# returned in 240 ms, whereas it takes ~112 ms at 0.03 timeout (~105 ms at 0.025 timeout).
# T3Bi typical response time is less than 20 ms, so it may be rs232-USB bridge that
def init_comm():
    # port assignment done inside the respective classes.
    pass

def finish_comm():
    pass

class tvc():
    name, port, io, lock = 4*[None]
    def __init__(self, name='tvc', devid='/dev/t3bi'):
        self.name, self.port = name, devid
        self.io = serial.Serial(devid, baudrate=9600, timeout=0.05)
        self.lock = threading.RLock()

    _cmd_mod = '#' 
    def _cmd_io(self, cmdstr, eps=0):
        res = b''
        with self.lock:
            self.io.write(bytes(self._cmd_mod+cmdstr+'\n', 'utf-8'))
            if eps==0: return
            else: res = self.io.read(eps)#.inv[-1]
        if len(res)==0: TVC_Error('Timeout occurred', '')
        return res.decode("utf-8")
    _status_code = {'0': 'No error', '1': 'Unrecognised command', '2': 'Bad data value',\
            '3': 'Command ignored', '4': 'Reserved for future use'}
    p = {'units':'00', 'pcm':'0', 'actvch':'A', 'ss':'02', 'sig_V': '01', 
        'sig_Hrange': '0', 'sig_Lrange':'10', 'sens_state': 'LAD', 'valve_state': '0',
        'actv_setp': '1'}
    sp = {'1':['0', 0.0, 10000, 20], '2':['0', 0.0, 2000, 20], '3':['0', 0.0, 1000, 20], 
            '4':['0', 0.0, 1000, 20],'5':['0', 0.0, 1000, 20]}
    err, tmp = '0', None
    
    def rev(d): return dict([[v,k] for k,v in mydict.items()])
    # in the following dicts, only the seemingly useful values are kept. See manual for more.
    d1={'PID':'1', 'Model':'0'}; d1i=rev(d1)
    d2={'torr':'00', 'mbar':'02', 'kPa':'05'}; d2i=rev(d2)
    d3={'Auto':'A', 'High':'H', 'Low':'L'}; d3i=rev(d3) # High = high pressure sensor, CH1, Low range pressure=CH2
    d4={'Open':'0', 'Close':'1'}; d4i=rev(d4)
    d5={'1V':'0','5V':'1','10V':'2'}; d5i=rev(d5) # input signal voltage
    d6={'1':'03','10':'06','100':'08','1000':'10'}; d6i=rev(d6) # sensor range in torr
    s1={'A':'1','B':'2','C':'3','D':'4','E':'5'}; s1i=rev(s1) # setpoint number
    s2={'Position':'0','Pressure':'1'}; s2i=rev(s2) # setpoint mode, pressure is default for all setpoints.
    s3={'A':'25','B':'26','C':'27','D':'28','E':'29'}; s4={'A':'1','B':'2','C':'3','D':'4','E':'10'} # setpoint rev numbering
    s5={'A':'46','B':'47','C':'48','D':'49','E':'50'}; s6={'A':'41','B':'42','C':'43','D':'44','E':'45'} # setpoint rev numbering

    io_table = { #preprocess code, respfmt, respargs, postprocess code
            # a stands for args and s for self in the strings of this table.
            # NI = not implemented
            # NI: stop calibrating valve (Q), reset (IX), Learn valve (J), Select Valve type (JT/RJT)
            "set_pcm":["a[0]=s.d1[a[0]]", "V{}", "{}V", ["s.err"], "s.p['pcm']=a[0]"], 
            "get_pcm":[None, "R51", "{}V{}", ["s.err", "s.p['pcm']"], "r=s.d1i[s.p['pcm']]"],
            "set_units":["a[0]=s.d2[a[0]]", "F{}", "{}F", ["s.err"], "s.p['units']=a[0]"], 
            "get_units":[None, "R34", "{}F{}", ["s.err", "s.p['units']"], "r=s.d2i[s.p['units']]"],
            # NI: backfill valve enable (BE0/1/RBE), Backfill limit (BL/RBL), Bf thresh. press. (MD/RMD)
            "set_actvch":["a[0]=s.d3[a[0]]", "L{}", "{}L", ["s.err"], "s.p['actvch']=a[0]"], 
            # NI: Learn (L), Set autoch. xover delay/point (LD/LHC/LLC/RD/RHC/RLC): LHC=0.9, LLC=100 by default
            # Unit switches to SafeState (SS) when a used pressure sensor is disconnected
            "set_ss":["a[0]=s.d4[a[0]]", "SS{}", "{}SS", ["s.err"], "s.p['ss']=a[0]"],
            "get_ss":[None, "RSS", "{}SS{}", ["s.err", "s.p['ss']"], "r=s.d4i[s.p['ss']]"],
            # NI: set Model based params (ST/R60-65), 
            "set_sig_V":["a[0]=s.d5[a[0]]", "G{}", "{}G", ["s.err"], "s.p['sig_V']=a[0]"],
            "get_sig_V":[None, "R35", "{}G{}", ["s.err", "s.p['sig_V']"], "r=s.d5i[s.p['sig_V']]"],
            "set_Hrange":["a[0]=s.d6[a[0]]", "EH{}", "{}EH", ["s.err"], "s.p['sig_Hrange']=a[0]"],
            "set_Lrange":["a[0]=s.d6[a[0]]", "EL{}", "{}EL", ["s.err"], "s.p['sig_Lrange']=a[0]"],
            "get_Hrange":[None, "R33", "{}EH{}", ["s.err", "s.p['sig_Hrange']"], "r=s.d6i[s.p['sig_Hrange']]"],
            "get_Lrange":[None, "R55", "{}EL{}", ["s.err", "s.p['sig_Lrange']"], "r=s.d6i[s.p['sig_Lrange']]"],
            # NI: Speedup lowpass filter, filter, time (SUE/SUF/SUT/RUE/RUF/RUT), 
            # NI: pump speed pedastal (SCP/RCP), Chamber vol.+estimator (SVO/SVE/RVO/RVE)
            
            # setpoint related, analog setpoint is not used. PID mode is used.
            # For position mode of setpoint, do not set/read gain, phase. Errors are not handled.
            "set_mode":["a[0]=s.s1[a[0]]; a[1]=s.s2[a[1]]", "T{}{}", "{}T", ["s.err"], "s.sp[a[0]][0]=a[1]"],
            "get_mode":["a[0]=s.s3[a[0]]", "R{}", "{}T{:1}{}", ["s.err", "s.tmp", "s.sp[s.s1i[s.tmp]][0]"], 
                "r=s.setp[s.s1i[s.tmp]][0]"],
            "set_val":["a[0]=s.s1[a[0]]", "S{}{}", "{}S", ["s.err"], "s.sp[a[0]][1]=a[1]"],
            "get_val":["a[0]=s.s4[a[0]]", "R{}", "{}S{:1}{:g}", ["s.err", "s.tmp", "s.sp[s.s1i[s.tmp]][1]"], 
                "r=s.setp[s.s1i[s.tmp]][1]"],
            "set_gain":["a[0]=s.s1[a[0]]", "M{}{}", "{}M", ["s.err"], "s.sp[a[0]][2]=a[1]"], 
            "get_gain":["a[0]=s.s5[a[0]]", "R{}", "{}M{:1}{:d}", ["s.err", "s.tmp", "s.sp[s.s1i[s.tmp]][2]"], 
                "r=s.sp[s.s1i[s.tmp]][2]"],
            "set_phase":["a[0]=s.s1[a[0]]", "X{}{}", "{}X", ["s.err"], "s.sp[a[0]][3]=a[1]"], 
            "get_phase":["a[0]=s.s6[a[0]]", "R{}", "{}X{:1}{:d}", ["s.err", "s.tmp", "s.sp[s.s1i[s.tmp]][3]"], 
                "s.sp[s.s1i[s.tmp]][3]"],
            # NI: GCF, PCF (GC/PC/RGC/RPC), softstart rates (I), Process Limit relay/sources (PLR/S), 
            "activate_setp":["a[0]=s.s1[a[0]];", "D{}", "{}D", ["s.err"], None],
            "get_state":[None, "R7", "{}M{}{}{}{}", ["s.err", "s.p['actv_setp']", "s.p['valve_state']",
                "self.tmp", "self.p['sens_state']"], None],
            "open":[None, "O", "{}", ["s.err"], None],
            "close":[None, "C", "{}", ["s.err"], None],
            "hold":[None, "H", "{}", ["s.err"], None],
            # NI: Sensor/special zero (Z1, Z2, Z3), Calibrate (Y3/Y4/R52), open/close backfill relay (CB/OB/RB)
            "read_press":[None, "R5", "{}P{:g}", ["s.err", "s.tmp"], None],
            "read_pos":[None, "R6", "{}V{:g}", ["s.err", "s.tmp"], None],
            # NI: system status (R37)
            "read_ver":[None, "R38", "{:1}{}", ["s.err", "s.tmp"], None],
            "read_chksum":[None, "R52", "{}CS{}", ["s.err", "s.tmp"], None],
    }
    func_table = {"init_state": init_state, "get_pressure_position": get_pressure_position}
    
    def io(s, cmd, args):
        # how this works: find the cmd and resp fmt using "cmd".
        # fill in cmdfmt with supplied arguments, then do an io
        # with the device. resp fmt is used to update the class's
        # parameter. pre and post processing code chunks are run if they are present.
        r = ''
        if _DEBUG: print("tvc:", cmd, args)
        if cmd in s.io_table.keys(): pre, cfmt, rfmt, rargs, post = s.io_table[cmd]
        else: return s.func_table[cmd](s, args)
        if pre is not None: exec(pre)
        cmdstr = bytes(cfmt.format(*args), "utf-8")
        resp = s._cmd_io(cmdstr)
        if rfmt != "":
            for i in range(len(rargs)): # this ugly hack since python doesn't allow ref passing
                if type(res[i])==type(''): x = "'"+res[i]+"'"
                else: x = str(res[i])
                exec(rargs[i]+"="+x)
        if post is not None: # use of ldict is *not* needed for python2.X
            ldict = {"s":s}; exec(post, globals(), ldict); r = ldict["r"]
        return [r]

    def init_state(self, args):
        # open or close the valve
        if args['manual-setp']=='Open': self._valve_open()
        # set the sensor ranges
        if args['reset_sensor_range']:
            self._set_sensor_high_range(args['high_sensor_fs_range'])
            self._set_sensor_low_range(args['low_sensor_fs_range'])
        self.get_sensor_ranges([])
        # see if slowpump is enabled, set accordingly.
        self.slowpump = args['slowpump']
        #_set_slowpump_rate(self.slowpump['rate'])
        #_set_slowpump_enable(self.slowpump['state'])
        #_set_slowpump_pressure(self.slowpump['pressure'])
        # set safety state
        self.fallback_state = args['fallback_state']
        self._set_ss(self.fallback_state)
        # set crossover parameters
        #get and set the setpoint values
        for s in args['setpoints']:
            self.setps[s[0]] = s[1:]
            self._set_setp_values(s[0], self.setps[s[0]])
        # activate a setpoint if needed
        if args['actv_setpoint'] != 'manual': self.activate_setp(args['actv_setpoint'])
        #self.get_actv_setp_ch([])

        #other initialisation steps are
        # self._set_sensor_low_range('EL03')  # for low range sensor at 1 torr.
        # self._set_sensor_high_range('EH10') # for high sensor range to 1000 torr
        # self._set_xover_delay(90) # change the delay for autocrossover
        # self._set_high_chan_xover_pt(0.9) # change the % of FS range of high sensor when crossover occurs.
        # self._set_low_chan_xover_pt(100) # similar for low gauge

    def get_pressure(self, args):
        return 0, [self._get_valve_pressure(self.actv_ch, self.high_fs, self.low_fs)]
    def get_position(self, args):
        return 0, [self._get_valve_position()]
    def get_pressure_position(self, args):
        # the combined commands reduce the time taken to 40 ms, compared to
        # 150 ms for doing them separately. It also introduces some problems
        # in getting response on time.
        #p, v, st = 0, 0, self.state
        #try: p, st = _cmd_io('R5\nR7').split('\n', 2)
        # if len(st)>5, then we are reading another response, which will
        # cause problem in next request. Don't know how to get around this.
        #except KeyError: # because cmd_io doesn't get a '0' at start.
        #    lst = _cmd_io('R5\nR6\nR7').split('\n')
        #    for l in lst:
        #        if l[0] == 'V': v = l
        #        elif l[0] == 'M': st = l
        #        elif l[0] == 'P' or l[1] == 'P': p = l
        
        #p, st = _cmd_io('R5\nR7').split('\n',1) # this is done to make response fast.
        #print st
        #self.state = st
        #self.actv_ch = _sensor_ch_status[st[4]][0]
        #if self.actv_ch == 'H': p = 0.01*float(p[2:])*self.high_fs
        #else: p = float(p[2:])*self.low_fs

        # there is no need to determine which channel is in use when the mode
        # is auto. In this case following formula works alright.
        # the code above was written assuming that I have to find out which
        # channel is in use to determine the fs range being referred to.
        p = self._cmd_io('R5')
        p = 0.01*float(p[2:])*self.high_fs
        v = float(self._cmd_io('R6')[2:])
        #self.actv_ch = _get_valve_state()['Active Sensor'][0]
        #p1, p2 = _get_valve_pressure(self.actv_ch, self.high_fs, self.low_fs), _get_valve_position()
        #return 0, ['%.3f/%.2f'%(p1, p2)]
        return 0, ['%.5f'%(p), '%.4f'%(v)]
    def valve_open(self, args):
        self._valve_open()
        return 0, [1]
    def valve_close(self, args):
        self._valve_close()
        return 0, [0]
    def get_setp_state(self, args):
        setpname = args[0]
        if setpname not in self.setps:
            return -1, [setpname]
        self.setps[setpname] = self._get_setp_values(setpname)
        return 0, [setpname, self.setps[setpname]]
    def set_setp_state(self, args):
        """args: setpointname mode value"""
        setpname, mode, v1, v2 = args[0], args[1], float(args[2]), float(args[3])
        if setpname not in self.setps or mode not in self._setp_mode.values() or \
                not (0 <= v1 <= 100) or not (0.1 <= v2 <= 100):
            return -2, [setpname+' '+mode+' '+repr(v1)+' '+repr(v2)]
        values = [mode, v1, v2]
        self._set_setp_values(setpname, values)
        self.setps[setpname] = values
        return 0, [setpname, values]
    def get_sensor_ranges(self, args):
        self.high_fs = float(self._get_sensor_high_range()[:-5]) #.inv[-5])
        self.low_fs = float(self._get_sensor_low_range()[:-5]) #.inv[-5])
        return 0, [self.high_fs, self.low_fs]
    def get_actv_setp_ch(self, args):
        res = _get_valve_state()
        self.actv_setp = res['Setpoint']
        self.valve_state = res['Valve']
        self.actv_ch = res['Active Sensor'][0]
        return 0, [self.actv_setp]
    def activate_setp(self, args):
        resp = self._activate_setp(args[0])
        # check for errors!
        self.actv_setp = args[0]
        return 0, [self.actv_setp, self.setps[self.actv_setp]]
    def set_setp_params(self, args):
        setpname, i1, i2 = args[0], int(args[1]), int(args[2])
        if setpname not in self.setps or not (1 <= i1 <= 32767) or not (1 <= i2 <= 32767):
            return -3, [setpname+' '+repr(i1)+' '+repr(i2)]
        self._set_setp_gain(setpname, i1)
        self._set_setp_phase(setpname, i2)
        return 0, [setpname, i1, i2]
    def get_setp_params(self, args):
        setpname = args[0]
        if setpname not in self.setps:
            return -1, [setpname]
        self.setp_params[setpname] = [self._get_setp_gain(setpname), self._get_setp_phase(setpname)]
        return 0, [setpname, self.setp_params[setpname]]
    def _get_all_state(self):
        return {
                'Valve type': self._get_valve_type(),
                'Pressure control mode': self._get_press_cntrl_mode(),
                'Pressure units': self._get_press_units(),
                'Crossover params, A(ms), H(Torr), L(Torr)': [self._get_auto_chan_xover_delay(),\
                        self._get_high_chan_xover_pt(), self._get_low_chan_xover_pt()],
                'Pressure ranges (Torr), H, L': [self._get_sensor_high_range(), self._get_sensor_low_range()],
                'Sensor signal-input range': self._get_sensor_sigin_range(),
                'Setpoints': self._get_setp_status(),
                'Valve State': self._get_valve_state(),
                'Valve control state': self._get_cntrl_state(),
                'Slowpump enabled with rate': [self._get_slowpump_enable(), self._get_slowpump_rate()],
                'Valve, encoder position': [self._get_valve_position(), self._get_encoder()]
                }

    def _get_setp_status(self, s=None):
        slist = ['A','B', 'C', 'D', 'E']
        if s is not None: slist = [s]
        state = {}
        for x in slist: state[x] = self._get_setp_values(x)
        if s is None: state['Analog'] = self._get_analog_setp_range()
        return state

    def _valve_open(self): return self._cmd_io('O', eps=0)
    def _valve_close(self): return self._cmd_io('C', eps=0)
    def _valve_hold(self): return self._cmd_io('H', eps=0)
    def _stop_calib_valve(self): return self._cmd_io('Q', eps=0)
    def _reset(self): return self._cmd_io('IX', eps=0)
    def _get_valve_type(self): return self._cmd_io('RJT')
    _press_cntrl_modes = bidict({'1':'PID', '0':'Model'})
    def _get_press_cntrl_mode(self): return self._press_cntrl_modes[self._cmd_io('R51')[1]]
    def _set_press_cntrl_mode(self, m):
        try: return self._cmd_io('V'+self._press_cntrl_modes.inv[m])
        except KeyError: return 'Allowed modes are PID|Model, given:'+m
    _p_units = bidict({ '00':'Torr', '01':'mTorr', '02': 'mBar', '03': 'uBar', '04': 'kPa', '05': 'Pa', '06': 'cm H2O', '07': 'in H2O' })
    def _get_press_units(self): return self._p_units[self._cmd_io('R34')[1:3]]
    def _set_press_units(self, u):
        try: res = self._cmd_io('F'+self._p_units.inv[u]); return self._p_units[res[1:3]]
        except KeyError: return 'Allowed units are Torr|mTorr|mBar, given:'+u
    def _learn_system(self): return self._cmd_io('L', eps=0)
    #def learn_valve_steps(): _cmd_io('J'); return '' #only needed after repairs
    _actv_chan = bidict({'A':'Auto', 'H':'High', 'L':'Low'}) # high = ch1, low = ch2
    _valve_status = bidict({'0':'controlling', '2':'open', '4':'close'})
    _pressure_status = bidict({'0':'<=10% FS', '1':'>10% FS'})
    _sensor_ch_status = bidict({ '0': 'LAD', '1':'HAD', '3':'HHD', '4':'LAE',\
            '5':'HAE', '7':'HHE', '8':'LLD', ':':'LLE'})
    def _get_valve_state(self):
        s = self._cmd_io('R7')
        return {'Setpoint': self._setp_ctrl_resp[s[1]], 'Valve': self._valve_status[s[2]],
                'Pressure': self._pressure_status[s[3]], 'Active Sensor': self._sensor_ch_status[s[4]] }
    def _set_actv_chan(self, c):
        try: return self._cmd_io('L'+self._actv_chan.inv[c])
        except KeyError: return 'Allowed channels are Auto|High|Low, given:'+c
    def _set_auto_chan_xover_delay(self, ms):
        i = int(ms)
        if i < 0 or i > 999: return 'Time should be 0<t<1000, given:'+ms
        return self._cmd_io('LD%03i'%(i)) #100 msec default, leave as it is.
    def _get_auto_chan_xover_delay(self): return {'Delay (ms):': self._cmd_io('RD')[2:]}
    # following is 0.9% of full scale (i.e. 1000 torr in our case). We want it to be ~ 0.1
    def _set_high_chan_xover_pt(self, v):
        f = float(v)
        if f < 0 or f > 1: return 'A number <1 is required, given:'+v
        return self._cmd_io('LHC%.1f'%(f))
    def _get_high_chan_xover_pt(self): return {'High channel x-over pt (in % of high)': self._cmd_io('RHC')[2:]}
    def _set_low_chan_xover_pt(self, v): 
        i = int(v)
        if i < 0 or i > 100: return 'A number 0<x<100 is required, given:'+v
        return self._cmd_io('LLC%03i'%(i))
    def _get_low_chan_xover_pt(self): return {'Low channel x-over pt (in % of high)': self._cmd_io('RLC')[2:]}
    _safety_states = bidict({'0':'Open', '1':'Close', '2':'Hold', '3': 'Safe', '4': 'Cycle'})
    def _set_ss(self, state): 
        try: return self._cmd_io('SS'+self._safety_states.inv[state])
        except KeyError: return 'Allowed states are Open|Close|Hold|Safe|Cycle, given:'+state
    def _get_ss(self): return {'Valve safety state': self._safety_states[self._cmd_io('RSS')[2]]} 
    # sensor must be connected before changing the sigin-range.
    _sensor_sigin_ranges = bidict({'0':'1', '1':'5', '2':'10'})
    def _set_sensor_sigin_range(self, r): 
        try: return self._cmd_io('G'+self._sensor_sigin_ranges.inv[r])
        except KeyError: return 'Allowed ranges are 1|5|10, given:'+r
    def _get_sensor_sigin_range(self): return repr(self._sensor_sigin_ranges[self._cmd_io('R35')[1]])+' V'
    _sensor_ranges = bidict({'03':'1 Torr', '06': '10 Torr', '08': '100 Torr', '10': '1000 Torr'}) # using only ranges we need.
    def _set_sensor_low_range(self, r): 
        # to change the range of the gauges attached on T3Bi, one has
        # to enter the calibration mode. If any other command yields
        # "command ignored" response, check if it needs Calib. mode.
        try:
            resp = self._cmd_io('CAL1234') # to enter protected mode.
            resp0 = self._cmd_io('EL'+_sensor_ranges.inv[r+' Torr'])
            resp = self._cmd_io('USR') # come back to user mode.
            return resp0
            # _cmd_io('ROM') can be used to check the mode of instrument.
        except KeyError: return 'Allowed ranges are 1|10|1000, given:'+r
    def _get_sensor_low_range(self): return self._sensor_ranges[self._cmd_io('R55')[2:4]] # initial is 10 torr
    def _set_sensor_high_range(self, r):
        try:
            resp = self._cmd_io('CAL1234')
            resp0 = self._cmd_io('EH'+_sensor_ranges.inv[r+' Torr'])
            resp = self._cmd_io('USR')
            return resp0
        except KeyError: return 'Allowed ranges are 1|10|1000, given:'+r
    def _get_sensor_high_range(self): return self._sensor_ranges[self._cmd_io('R33')[2:4]] # initial is 1000 torr
    # sets the minimum valve position for pressure control mode, 0-30 %, 0 is default.
    def _vset_pump_speed_pedestal(self, v): 
        f = float(v)
        if f < 0 or f > 30: return 'A number 0<x<30 is required, given:'+v
        return self._cmd_io('SCP%02i'%(f))
    def _get_pump_speed_pedestal(self): return self._cmd_io('RCP')
    
    # Speedup compensation parameter: used to compensate for measurement delays due
    # to gauges. there is one compensation constant and one filter constant (5-10
    # times smaller than former). 
    def _speedup_compensator(self, i): return self._cmd_io('SUE%1i'%(i))
    def _get_speedup_compensator(self): return self._cmd_io('RUE')
    def _set_speedup_constant(self, v): 
        f = float(v)
        if f < 0.01 or f > 0.1: return 'A number 0.01<x<0.1 is required, given:'+v
        return self._cmd_io('SUT%1.2f'%(f))
    def _get_speedup_constant(self): return self._cmd_io('RUT')
    def _set_speedup_filter(self, v): 
        f = float(v)
        if f < 0.01 or f > 0.1: return 'A number 0.01<x<0.1 is required, given:'+v
        return self._cmd_io('SUF%1.2f'%(f))
    def _set_speedup_filter(self): return self._cmd_io('RUF')

    # how does analog setpoint work?
    # Following a dozen functions can be compressed in 2 if the codes used
    # in commands and response were uniform. 
    _setp_ctrl_cmd = bidict({'1':'A', '2':'B', '3':'C', '4':'D', '5':'E', '6':'analog', '7':'open', '8':'close'})
    _setp_ctrl_resp = bidict({'0':'analog', '1':'A', '2':'B', '3':'C', '4':'D', '5':'E', '6':'open', '7':'close', '8':'stop', '9':'learning'})
    _setp_ctrl_req = bidict({'25':'analog', '26':'A', '27':'B', '28':'C', '29':'D', '30':'E'})
    _setp_phase_req = bidict({'41':'A', '42':'B', '43':'C', '44':'D', '45':'E', '53':'analog'})
    _setp_gain_req = bidict({'46':'A', '47':'B', '48':'C', '49':'D', '50':'E', '54':'analog'})
    _setp_proc_lmt_relays = bidict({'1':'lowPL1', '2':'highPL1', '3':'lowPL2', '4':'highPL2'})
    _setp_proc_lmt_relays_req = bidict({'11':'lowPL1', '12':'highPL1', '13':'lowPL2', '14':'highPL2'})
    _setp_mode = bidict({'0': 'Position', '1':'Pressure'})
    _analog_setp_ranges = bidict({'0':'+-5', '1':'+-10'})
    _setp_ctrl1 = bidict({'0':'analog', '1':'A', '2':'B', '3':'C', '4':'D', '5':'E', '6':'open', '7':'close', '8':'stop', '9':'learning'})
    
    def _set_setp_mode(self, spoint, mode): 
        try: 
            s = self._cmd_io('T'+self._setp_ctrl_cmd.inv[spoint]+self._setp_mode.inv[mode])
            return self._setp_ctrl_resp[s[1]], self._setp_mode[s[2]]
        except KeyError: return 'Setpoints are A|B|C|D|E|analog, modes are pressure|position. Given:'+spoint+mode
    def _get_setp_mode(self, spoint):
        try: s = self._cmd_io('R'+str(self._setp_ctrl_req.inv[spoint]))
        except KeyError: return 'Setpoints are A|B|C|D|E|analog. Given:'+spoint
        #return _setp_ctrl_resp[s[1]], _setp_mode[s[2]]
        return self._setp_mode[s[2]]
    def _set_analog_setp_range(self, v):
        if v not in ['5', '10']: return 'Analog setpoint ranges can be 5|10 V, given:'+v
        return self._cmd_io('A'+self._analog_setp_ranges.inv['+-'+v])
    def _get_analog_setp_range(self): 
        s = self._cmd_io('R24')
        return self._analog_setp_ranges[s[1]]
    def _set_setp_value(self, spoint, v):
        f = float(v)
        try: s = self._setp_ctrl1.inv[spoint]
        except KeyError: return 'Setpoint should be A|B|C|D|E|analog, value should be 0<x<100. Given:'+spoint+v
        if s==6 and int(f) not in [0, 1]: return 'Value for analog setpoint should be 0|1, given:'+v
        elif not 0 <= f <= 100: return 'Value for setpoints should be 0<=x<=100, given:'+v
        s1 = self._cmd_io('S'+s+repr(f))
        #return _setp_ctrl_resp[s1[1]], s1[2:]
        return float(s1[2:])
    def _get_setp_value(self, spoint):
        d = bidict({'0':'analog', '1':'A', '2':'B', '3':'C', '4':'D', '10':'E'})
        try: s = self._cmd_io('R'+d.inv[spoint])
        except KeyError: return 'Setpoint should be A|B|C|D|E|analog, given:'+spoint
        #return _setp_ctrl_resp[s[1]], s[2:]
        return float(s[2:])
    def _set_setp_gain(self, spoint, g):
        i = int(g)
        try: s = self._setp_ctrl1.inv[spoint]
        except KeyError: return 'Setpoint should be A|B|C|D|E|analog, value should be 0<x<32767, given:'+spoint+g
        if not 1 <= i <= 32767: return 'Value for setpoint gain should be 0<=x<=32767, given:'+g
        s1 = self._cmd_io('M'+s+repr(i))
        #return self._setp_ctrl_resp[s1[1]], s1[2:]
        return float(s1[2:])
    def _get_setp_gain(self, spoint):
        try: s = self._cmd_io('R'+self._setp_gain_req.inv[spoint])
        except: return 'Setpoint should be A|B|C|D|E|analog, given:'+spoint
        #return _setp_ctrl_resp[s[1]], s[2:]
        return float(s[2:])
    def _set_gain_compenstation_factor(self, v): pass # not implemented yet
    def _set_setp_phase(self, spoint, p): 
        i = int(p)
        try: s = self._setp_ctrl1.inv[spoint]
        except KeyError: return 'Setpoint should be A|B|C|D|E|analog, value should be 0<x<32767, given:'+spoint+p
        if not 1 <= i <= 32767: return 'Value for setpoint phase should be 0<=x<=32767, given:'+p
        s1 = self._cmd_io('X'+s+repr(i))
        #return self._setp_ctrl_resp[s1[1]], s1[2:]
        return float(s1[2:])
    def _get_setp_phase(self, spoint):
        try: s = self._cmd_io('R'+self._setp_phase_req.inv[spoint])
        except KeyError: return 'Setpoint should be A|B|C|D|E|analog, given:'+spoint
        #return self._setp_ctrl_resp[s[1]], s[2:]
        return float(s[2:])
    def _set_phase_compenstation_factor(self, v): pass
    def _set_softstart_rate(self, spoint, v):
        f = float(v)
        try: s = self._setp_ctrl1.inv[spoint]
        except KeyError: return 'Setpoint should be A|B|C|D|E|analog|open|close, value should be 0.1<=x<=100, given:'+spoint+v
        if not 0.1 <= f <= 100.0: return 'Value for setpoint softstart should be 0.1<=x<=100, given:'+v
        s1 = self._cmd_io('I'+s+repr(f))
        #return self._setp_ctrl_resp[s1[1]], s1[2:]
        return float(s1[2:])
    _setp_softstart_req = bidict({'15':'A', '16':'B', '17':'C', '18':'D', '19':'E', '20':'analog', '21':'open', '22':'close'})
    def _get_softstart_rate(self, spoint):
        try: s = self._cmd_io('R'+self._setp_softstart_req.inv[spoint])
        except KeyError: return 'Setpoint should be A|B|C|D|E|analog|open|close, given:'+spoint
        #return _setp_ctrl_resp[s[1]], s[2:]
        return float(s[2:])
    def _get_setp_values(self, s, brief=True):
        if brief: return [self._get_setp_mode(s), self._get_setp_value(s), self._get_softstart_rate(s)]
        else: return [self._get_setp_mode(s), self._get_setp_value(s), self._get_softstart_rate(s),\
                self._get_setp_gain(s), self._get_setp_phase(s)]
    def _set_setp_values(self, s, v, brief=True):
        if brief: return [self._set_setp_mode(s, v[0]), self._set_setp_value(s, v[1]), self._set_softstart_rate(s, v[2])]
        else: return [self._set_setp_mode(s, v[0]), self._set_setp_value(s, v[1]), self._set_softstart_rate(s, v[2]),\
                self._set_setp_gain(s, v[3]), self._set_setp_phase(s, v[4])]
    def _activate_setp(self, spoint):
        try: return self._cmd_io('D'+self._setp_ctrl_resp.inv[spoint])[1]
        except: return 'Setpoints are A|B|C|D|E|analog. Given:'+spoint
    # to get active setpoint, use get_valve_state

    #def sensor_zero(): self._cmd_io('Z1'); return recv()
    #def special_zero(): self._cmd_io('Z2'); return recv()
    #def remove_zeros(): self._cmd_io('Z3'); return recv()
    
    _slowpump_conds = bidict({'0':'disable', '1':'both', '2':'dec', '3':'inc'})
    # gentle pump in torr/s.
    # not sure what the recipenum in the documents mean. Hopefully following
    # interpretation is correct.
    def _set_slowpump_pressure(self, v, recipenum=_slowpump_conds.inv['both']):
        # the pressure in % of high pressure sensor.
        f = float(v)
        if not 0 <= f <= 100: return 'FS range should be between 0 and 100, got:'+repr(v) 
        return self._cmd_io('S%c%.1f'%(recipenum, f))[2:]
    def _set_slowpump_rate(self, v):
        f = float(v)
        if f <= 0: return 'Rate (Torr/s) should be >0, got:'+repr(v)
        return self._cmd_io('SR%.1f'%(f))[2:]
    def _get_slowpump_rate(self): s = self._cmd_io('RSR'); return s[2:]
    def _set_slowpump_enable(self, c): 
        try: return self._slowpump_conds[self._cmd_io('SE'+self._slowpump_conds.inv[c])[2]]
        except KeyError: return 'Slowpump condition should be disable|inc|dec|both, given:'+c
    def _get_slowpump_enable(self): return self._slowpump_conds[self._cmd_io('RSE')[2]]
    
    def _get_valve_pressure(self, actv_ch, h_range, l_range):
        v1 = self._cmd_io('R5')
        print(v1)
        for i in range(len(v1)):
            if v1[i] == 'P': break
        v = float(v1[i+1:])
        if actv_ch == 'H': return 0.01*v*h_range
        return 0.01*v*l_range
    def _get_valve_position(self): return float(self._cmd_io('R6')[1:])
    def _get_encoder(self): return self._cmd_io('REN')[2:]  # how is encoder different than valve position?
    def _get_interlock(self): return self._cmd_io('RIN')[2]


    _mode = {'0':'local', '1':'remote'}
    _learn_state = {'0':'not learning', '2':'learning'}
    _valve_ctrl = {'0':'open', '1':'close', '2':'stop', '3':'A', '4':'B',\
            '5':'C', '6':'D', '7':'E', '8':'analog'}
    def _get_cntrl_state(self):
        s = self._cmd_io('R37')
        return {'Mode': self._mode[s[1]], 'Learn': self._learn_state[s[2]], 'Valve': self._valve_ctrl[s[3]]}
    def _get_version(self, ): return {'Firmware Ver':self._cmd_io('R38'), 'Build': self._cmd_io('R66')}
    _cksum_ok = {'0': 'OK', '1': 'Error'}
    def _get_cksum(self): return self._cksum_ok[self._cmd_io('R52')[2]]


    # Backfill feature: when pressure is changing to a new setpoint, the backfill
    # signal can be generated on one of the I/O pins to activate a pneumatic valve,
    # to help in changing pressure.
    # Pressure limit (for stopping this feature), pressure threshold (at which point
    # it activates) and delay can be changed. We don't use this feature, although
    # this can be connected to valve at the pump.
    
    # Learn the system feature: the valve can learn pumping speed curve for the system
    # and use it in better control. This is called "Model based control". 



class TVC_Error(Exception):
    def __init__(self, value, dat):
        self.value = value + repr(dat)
    def __str__(self):
        return repr(self.value)


# to test:
# tvc.init_comm()
# tvc.get_version()
# tvc.get_status()
# tvc.get_position()
# tvc.get_press()

