from parse import parse


def funcmap(devname, devid):
    m = n112(devname, devid)
    c = ['init_state', 'get_flow', 'set_flow', 'get_fs_range']
    return c, m.errcodes

#_TEST = False
_DEBUG = True # turn this on for debug output on console.

import serial 
import threading
_baud_rates = [38400]
_tm_o = {38400: 0.1, 9600: 0.1}
# we are working on Linux/Rpi. Otherwise use 'COM?' for portname on Win.
chan_d = {'lock':threading.RLock(), 'portname':'/dev/mfcs', 'port': None, 'cbr': 38400, 'timeout':_tm_o[38400], 'error_attempts':9}
# DMFC only handle upto 9 error responses.
def port_close(): chan_d['port'].close()
def init_comm(dummydev=False):
    if dummydev==True: chan_d['port'] = emuDev()
    else: 
        # 1 start bit, 1 stop bit, 7 char bits, odd parity.
        chan_d['port'] = serial.Serial(chan_d['portname'], baudrate=chan_d['cbr'], 
                parity=serial.PARITY_ODD, timeout=chan_d['timeout'], bytesize=serial.SEVENBITS)
    
_start, _pad, _stx, _etx, _enq, _ack, _lf, _cr, _nak, _star, _at = \
    b'@', b'\x00', b'\x02', b'\x03', b'\x05', b'\x06', b'\x0A', b'\x0D', b'\x15', b'\x2A', b'\x40'
    #spl_chars = {'STX':'\x02', 'ETX':'\03', 'ENQ':'\x05', 'ACK':'\06', 'LF':'\x0A', 'CR':'\x0D', 'NAK':'\x15'}
    #_STX, _ETX, _ENQ, _ACK, _LF, _CR, _NAK = '\x02', '\x03', '\x05', '\x06', '\x0A', '\x0D', '\x15'
_mps = 9 # minimum packet size
# note that device is appending \x00 after macid, so _mps is 10 instead of 9.
_ack_pkt_sz = 1

def bcc(x): return bytes([sum(x+_etx)%128])
def chk_resp_bcc(resp): return int.from_bytes(bcc(resp[1:-2]), byteorder='little') == resp[-1]

def form_cmd(func, data, macid):
    b = bcc(func+data)
    if b=='@': b += 3*_etx
    if b=='*': b += 9*_etx
    msg = _start + macid + _stx + func + data + _etx + b
    return msg

# wrapper for sending cmds and recving response
def cmd_io(cmdstr, attempts=chan_d['error_attempts']):
    _PORT = chan_d['port']
    _LOCK = chan_d['lock']
    if attempts==0: raise MFC_Error('MFC no response in 9 attempts')
    with _LOCK:
        _PORT.write(cmdstr)
        resp = b''
        while True:
            c = _PORT.read(1)
            #print(b'|'+c+b'|')
            if c==_nak: # incorrect bcc
                return cmd_io(cmdstr, attempts=attempts-1) 
            if c==_etx: 
                resp += _etx + _PORT.read(1) # read bcc
                break
            else: resp += c
            if c=='' or c==None: break
        #sleep(0.01) # 10 ms gap, as described in manual. omitted.
        ok, res = chk_resp_bcc(resp), resp[1:-2]
        if not ok: 
            _PORT.write(_nak)
            return cmd_io(cmdstr, attempts=attempts-1)
        _PORT.write(_ack)
        return res.decode("utf-8")

class n113():
    p = {"name": None, "macid": b'00', "fs_range": 0.0, "curr_setp": 0.0, "actv_flow": 0.0, "ramptime": 0.0, "units": 'B'}
    errcodes = {-1: 'Reuqested flow is beyond range'}
    unit_scale = {'A':0.91, 'B':1, 'C':1, 'D':1000} # to translate to sccm units 
    def __init__(self, name, macid):
        self.p['name'], self.p['macid'] = name, macid
    def init_state(self, args):
        self.p["fs_range"], self.p["curr_setp"] = args['fs_range'], args['init_val']
        #set_cbr(self.macid, br=chan_d['cbr'])
        cmd = form_cmd(b'\'!+REVERSE', b'', self.p['macid'])
        resp = cmd_io(cmd)
        #cmd = form_cmd(b'IAC', b'', self.macid)
        #resp = cmd_io(cmd)
        resp = self.io("set_flow", [self.p["curr_setp"]])
        return [resp]
    dev_funcs = { # cmd: ["cmdstr", "respfmt", [args], tr]
            "get_fs_range": ["RFS", "{:g},{}", ["s.p['fs_range']", "s.p['units']"], "r=s.p['fs_range']*s.unit_scale[s.p['units']]"],
            "get_flow": ["RFC", "{:f}", ["s.p['actv_flow']"], None],
            "set_flow": ["AFC{}", "", [], None],
            "get_addr": ["*)&", "{:d}", ["s.p['macid']"], None],          
            "set_addr": ["'*${}", "", [], None],
            "get_modelname": ["RMN", "", [], None],
            "get_mfgno": ["#)*", "", [], None],
            "get_version": ["RVR", "", [], None],
            "get_revision": ["RRV", "", [], None],
            "get_valve_state": ["ROC", "", [], None],
            "set_valve_state": ["MV{}", "", [], None],
            }
    compound_funcs = {"init_state": init_state}
    def io(s, cmd, args):
        # how this works: find the cmd and resp fmt using "cmd".
        # fill in cmdfmt with supplied arguments, then do an io
        # with the device. resp fmt is used to update the class's
        # parameter. pre and post processing code chunks are run if they are present.
        r = ''
        if _DEBUG: print("n112:", cmd, args)
        if cmd in s.dev_funcs.keys(): cfmt, rfmt, rargs, post = s.dev_funcs[cmd]
        else: return s.compound_funcs[cmd](s, args)
        cmdstr = bytes(cfmt.format(*args), "utf-8")
        r = cmd_io(form_cmd(cmdstr, b"", s.p["macid"]))
        if rfmt != "":
            res = parse(rfmt, r).fixed
            for i in range(len(rargs)): # this ugly hack since python doesn't allow ref passing
                if type(res[i])==type(''): x = "'"+res[i]+"'"
                else: x = str(res[i])
                exec(rargs[i]+"="+x)
        if post is not None: # use of ldict is *not* needed for python2.X
            ldict = {"s":s}; exec(post, globals(), ldict); r = ldict["r"]
        return [r]
        
class emuDev():
    p = {'flow': 0.0, 'units': 'B', 'addr': 0, 'valve_state': ''}  # parameters
    resp = ''
    io_table0 = { # parsed cmd: [readstr, readargs, writestr, writeargs]
            'RFS': ['', None, '100,D', None],       # get fs_range
            'RFC': ['', None, '{}', ['flow']],   # get flow
            'AFC': ['{:f}', ['flow'], '', None], # set flow
            '#)*': ['', None, 'XXX', None],         # get mfg no.
            '*)&': ['', None, '{:d}', ['addr']], #get addr
            '\*$': ['{:d}', ['addr'], '', None], #set addr
            'RMM': ['', None, 'N112', None],        #get model name
            'RRV': ['', None, '1.2.3', None],       #get revision
            'RVR': ['', None, '2', None],           #get version
            'MV': ['{}', ['valve_state'], '', None], #set valve state
            'ROC': ['', None, '{}', ['valve_state']],#get valve state
            #'ASS': ['', None, 'Not Impl', None]    #set sweeptime
            }
    def io_table(self, k): return self.io_table0[k] if k in self.io_table0.keys() else ['', None, 'NotImpl', None]

    def write(self, cmdstr):
        if _DEBUG: print('emu:write1', cmdstr)
        # no checks for bcc or the proper _start, macid, _stx etc. is made
        if cmdstr[0] == _ack: return
        f_d = cmdstr[4:-2].decode("utf-8")
        if f_d[:2] == 'MV': f, d = f_d[:2], f_d[2:]
        else: f, d = f_d[:3], f_d[3:]
        i_fmt, i_args, o_fmt, o_args = self.io_table(f)
        print(i_fmt, d)
        if i_fmt != '':
            res = parse(i_fmt, d).fixed
            for i in range(len(i_args)): self.p[i_args[i]] = res[i]
        self.resp = o_fmt.format(*[self.p[x] for x in o_args]) if o_args else o_fmt
        self.resp = _start + bytes(self.resp, "utf-8") + _etx + bcc(bytes(self.resp, "utf-8"))
    def read(self, n):
        if self.resp=='': return '' # resp was not set due to incorrect cmdstr. will raise MFC_error after 9 attempts
        n = len(self.resp) if n > len(self.resp) else n 
        r, self.resp = self.resp[:n], self.resp[n:]
        if _DEBUG: print('emu:read1', r)
        return r
    def close(self): pass


if __name__=='__main__':
    init_comm(dummydev=True)
    m1 = n113('dummy', b'02')
    #r = mfc.init_state({'fs_range': 100, 'init_val': 0.0})
    #r = mfc.get_version([])
    #r = m1.io('set_flow', ['10.0'])
    #r = m1.io('get_flow', [])
    #r = m1.io('get_fs_range', [])
    #r = m1.io('get_mfgno', [])
    #r = m1.io('get_modelname', [])
    #r = m1.io('get_version', [])
    #r = m1.io('set_valve_state', ['O'])
    #r = m1.io('get_valve_state', [])
    #r = m1.io('init_state', {'fs_range':100, 'init_val':10.0})
    r = m1.io('get_fs_range', [])
    print(m1.p["fs_range"], m1.p["units"])
    print(r)

