This is an alternative implementation for the CVD Furance Controller that I
wrote for SRN Lab @ CeNSE IISc. This version uses websockets and HTML5 
techniques for improving performance and portability. Websockets replace the
ajax calls which were too heavy in last version, which will allow better
scaling to larger number of hardware components. The command/response format
is also chaged to use numbers instead of descriptive strings, which is again
more efficient in all respects. Webcomponents and google polymer has been used
to built GUI, which has made it modular, cleaner and extensible.

The code has not been tested on actual hardware which I will do soon. The
actual hardware handling codes need some changes. Minor tweaks in controller
and interface codes are needed to make it usable. SVG graphs also need to be
implemented with webcomponents, since we don't want to use the graph plotting
js libraries (to reduce dependecies and improve performance).

One motivation for all the changes was to prepare a framework which can handle
different kind of instruments, and I hope to use it in next few projects. It 
should also be fairly straightforward for others to use and extend.

Documentation will be provided for these goals in mind. The choice of the web
servers (e.g. plain websocket over wamp servers) and frameworks will be justified.

To run just type:
python interfaceserver.py 
in the main source code directory and in your browser go to: localhost:8080.

Dependencies: I am using Tornado web server, it should be installed as a python
library. bidict.py is included.

