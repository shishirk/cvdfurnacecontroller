#!/usr/bin/env python

"""Websocket server for the Instrument Controller.
"""

import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import uuid

import controlserver

from tornado.options import define, options

import socket
localaddr = socket.gethostbyname(socket.gethostname())
define("ws_ipaddr", default=localaddr, help="Use this ip address for websocket")
define("port", default=8888, help="run on the given port", type=int)

class Application(tornado.web.Application):
    def __init__(self):
        settings = dict(
            cookie_secret="bZJc2sWbQLKos6GkHn2VB92XwQt8S0R1kRvJ5/xJ89E=",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            login_url="/login",
            xsrf_cookies=False,
        )
        tornado.web.Application.__init__(self, [
            tornado.web.url(r"/", MainHandler, name="main"),
            tornado.web.url(r"/controllersocket", ControllerSocketHandler, name="controllersocket"),
            tornado.web.url(r'/login', LoginHandler, name="login"),
            tornado.web.url(r'/logout', LogoutHandler, name="logout"),
            ], **settings)

class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")

class MainHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("index.html", ws_ipaddr='localhost', ws_port=options.port) #, messages=ControllerSocketHandler.cache)
        #self.render("index.html", ws_ipaddr=options.ws_ipaddr, ws_port=options.port) #, messages=ControllerSocketHandler.cache)

class ControllerSocketHandler(tornado.websocket.WebSocketHandler):
    # waiters corresponds to the GUI frontends and are updated simultaneously.
    # todo: only localhost can issue commands, authorisation.
    waiters = set()
    client_ips = list()

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    def check_origin(self, origin): return True
    def open(self):
        # control server websocket access is auth as a part of main page access, 
        # so the following is not needed. It also doesn't work.
        #curr_user = self.get_secure_cookie("user_cookie")
        #if not curr_user:
        #    logging.info('invalid auth key')
        #    self.close(code=401, reason='invalid auth key')
        #    return
        logging.info("Opened connection from %s", self.request.remote_ip)
        ControllerSocketHandler.waiters.add(self)
        ControllerSocketHandler.client_ips.append(self.request.remote_ip)
        ipnumbers = ' '.join([str(x) for x in self.client_ips])
        logging.info("Now connected to %d clients (%s)", len(self.waiters), ipnumbers)

    def on_close(self):
        ControllerSocketHandler.waiters.remove(self)
        logging.info("Closed connection from ", self.request.remote_ip)

    @classmethod
    def send_results(cls, results):
        for waiter in cls.waiters:
            try: waiter.write_message(results)
            except: logging.error("Error sending message", exc_info=True)

    def on_message(self, message): 
        logging.info("> %r", message)
        parsed = tornado.escape.json_decode(message)
        logging.info(parsed)
        results = controlserver.execute_commands(parsed, client_info={'ip':self.request.remote_ip})
        jsonres = tornado.escape.json_encode(results)
        logging.info("< %r", jsonres)
        ControllerSocketHandler.send_results(jsonres)

class LoginHandler(BaseHandler):
    login_form = """<html><body>{}<form action="/login" method="post">
        Name: <input type="text" name="username"> Password: <input type="password" name="password"/><p>
        <input type="submit" value="Log in">
        </form></body></html>"""

    def get(self):
        self.write(self.login_form.format("Please Log in"))

    def check_permission(self, password, username):
        if username == "admin" and password == "admin": return True
        return False

    def post(self):
        username = self.get_argument("username", "")
        password = self.get_argument("password", "")
        auth = self.check_permission(password, username)
        if auth:
            self.set_current_user(username)
            self.redirect(self.get_argument("next", u"/"))
        else:
            self.write(self.login_form.format("Incorrect name or password. Please try again"))

    def set_current_user(self, user):
        if user: self.set_secure_cookie("user", tornado.escape.json_encode(user))
        else: self.clear_cookie("user")

class LogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect(self.get_argument("next", self.reverse_url("main")))

def main():
    tornado.options.parse_command_line()
    app = Application()
    app.listen(options.port)
    controlserver.SERVER_IPADDR = options.ws_ipaddr
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
