# this is a map of how devices are attached to channels.
#
# In a control system there will be many hardware channels (e.g.
# RS-232, RS-485, Ethernet, USB buses etc.). On each of those
# channels there can be multiple devices which are identified by
# a number or a tag. For example ethernet can be connected to 
# many devices which are identified by their IP addresses. We
# bundle all the code related to one hardware channel in one
# module and provide a map to functions of each of devices that
# channel has. This way, all channel specific code is in one
# place (example: channel init code), and locking of channel 
# can be done inside the module itself (when multiple clients
# are sending requests for the devices at the same time).
#
# In our case, throttle valve controller (tvc) occupies RS-232
# line, two GF40 MFCs occupy RS-485 bus, switches and MKS1179 MFC
# are on switches. The identification numbers are according to
# hardware/device configuration.
# Note that devid can be of any type (string, list, number). It is used 
# by the channel module and interpreted there.

_tvc = {
    'conn': 'tvc', 'devid': '/dev/t3bi',
    'init_params': {
        'manual-setp': 'Close', # this is the initial state
        'setpoints': [# name, mode, val, softstartval
            ['A', 'Position', 0.0, 100.0],
            ['B', 'Pressure', 0.0, 100.0],
            ['C', 'Position', 0.0, 100.0],
            ['D', 'Position', 0.0, 100.0],
            ['E', 'Position', 0.0, 100.0]
            # don't know how to use analog setpoint, so not listed here.
            ],
        'actv_setpoint': 'manual', # can be manual or A/B/C/D/E from above.
        'slowpump': {'state': 'both', 'rate': 20.0, 'pressure': 0.0},
        'fallback_state': 'Close',
        # high and low gauge ranges and auto crossover parameters can also
        # be set here. Needed only when you are changing gauges. See the init_params
        # function of tvc class in rs232.py file.
        # Currently it is set to 1000 Torr, 1 Torr, 100 msec xover delay, 0.1%
        # of high sensor xover point and 100% of low sensor xover point.
        'reset_sensor_range': False, # make this true to set the following ranges.
        'high_sensor_fs_range': 1000, 'low_sensor_fs_range': 1
        # 1000 torr and 10 torr are default ranges.
        }
    }
# change this "USB2" to whatever port rs485-USB converter is attached to. 
# min ranges are 250, 35, 35, 6 respectively
_mfc_n2_1 = {'conn':'mfc_n112', 'devid':b'01', 'init_params': {'fs_range': 1000.0, 'init_val': 0.1}}
_mfc_ar_1 = {'conn':'mfc_n112', 'devid':b'02', 'init_params': {'fs_range': 110.0, 'init_val': 0.1}}
_mfc_h2_1 = {'conn':'mfc_n112', 'devid':b'03', 'init_params': {'fs_range': 100.0, 'init_val': 0.1}}
_mfc_ch4_1 = {'conn':'mfc_n112', 'devid':b'04', 'init_params': {'fs_range': 22.0, 'init_val': 0.1}}

# the Festo pneumatic switch array is controlled by the GPIO pins of RPi. The
# Festo array has 24 switches numbered 1-24. RPi GPIO has pins 2-25 available
# for use, of which pins 2-24 correspond to Festo 2-24 and festo switch 1 is
# driven by GPIO 25.
# the devid is the pneumatic switch number which is translated to appropriate GPIO address internally. 
# Default is the default state of the
# switch (normally open or normall closed), we denote these by "flow" and "noflow".
# For BY-series switches, "noflow" is actually diverting the flow to another
# line. flow/noflow are mapped to 0,1 according to default state. 0 is always
# equal to default state.
_sw_before_inlet = {'conn':'switches', 'devid':['DIO', 1], 'init_params': {'default': 'noflow', 'init_val':'noflow'}}
_sw_ventline = {'conn':'switches', 'devid':['DIO', 2], 'init_params': {'default': 'noflow', 'init_val':'noflow'}}
_sw_ch4_1_in = {'conn':'switches', 'devid':['DIO', 3], 'init_params': {'default': 'noflow', 'init_val':'noflow'}}
_sw_ch4_1_by = {'conn':'switches', 'devid':['DIO', 4], 'init_params': {'default': 'noflow', 'init_val':'noflow'}}
_sw_h2_1_in = {'conn':'switches', 'devid':['DIO', 5], 'init_params': {'default': 'noflow', 'init_val':'noflow'}}
_sw_h2_1_by = {'conn':'switches', 'devid':['DIO', 6], 'init_params': {'default': 'noflow', 'init_val':'noflow'}}
_sw_ar_1_in = {'conn':'switches', 'devid':['DIO', 7], 'init_params': {'default': 'noflow', 'init_val':'noflow'}}
_sw_ar_1_by = {'conn':'switches', 'devid':['DIO', 8], 'init_params': {'default': 'noflow', 'init_val':'noflow'}}
_sw_n2_1_in = {'conn':'switches', 'devid':['DIO', 9], 'init_params': {'default': 'noflow', 'init_val':'noflow'}}
_sw_n2_1_by = {'conn':'switches', 'devid':['DIO', 10], 'init_params': {'default': 'noflow', 'init_val':'noflow'}}
_sw_pump_ctrl = {'conn':'switches', 'devid':['DIO', 11], 'init_params': {'default': 'noflow', 'init_val':'noflow'}}

devlist = {'tvc':_tvc,\
        'mfc-ch4-1':_mfc_ch4_1, 'mfc-ar-1':_mfc_ar_1,\
        'mfc-h2-1':_mfc_h2_1, 'mfc-n2-1':_mfc_n2_1, 
        'sw-before-inlet':_sw_before_inlet, 'sw-ventline':_sw_ventline,\
        'sw-ch4-1-in':_sw_ch4_1_in, 'sw-ch4-1-by':_sw_ch4_1_by, \
        'sw-ar-1-in':_sw_ar_1_in, 'sw-ar-1-by':_sw_ar_1_by, \
        'sw-h2-1-in':_sw_h2_1_in, 'sw-h2-1-by':_sw_h2_1_by, 
        'sw-n2-1-in':_sw_n2_1_in, 'sw-n2-1-by':_sw_n2_1_by, 'sw-pump-ctrl':_sw_pump_ctrl }


