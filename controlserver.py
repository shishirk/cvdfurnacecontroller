import socketserver
import json
#import msgpack
import threading
import os
import bidict

####Configure channels and devices.
import channels.tvc, channels.mfc_n112, channels.switches, channels.test_ch
import devconfig

# Channels refer to an interface with which hardware is accessed, e.g.
# several digital swtiches can be accessed from Labjack interface, some
# MFCs sit on RS485 line. Each of these interfaces may have several devices
# connected to them and are identifed by a device number (called devid 
# below), e.g. MFCs have a RS485 id. init_comm() function of the channels
# does common initialisation of the interface, e.g. for labjack, a USB
# port is opened and which is used by all the digital switches available
# on this labjack interface.
# In cases where the devices have their own ports, init_comm may be
# blank function, the ports will then be acquired by the init_state
# function of the devices. e.g. RS232 ports (accessed by USB interface)
# have a throttle valve controller and an MFC which use separate USB 
# ports. The ports to which these devices are bound are specified in
# devconfig.py by the user (it can also be auto detected, but is not 
# implemented at the moment.
chans = {'tvc':channels.tvc, 'mfc_n112':channels.mfc_n112, \
        'test_ch': channels.test_ch, 'switches':channels.switches} 
for k,v in chans.items(): v.init_comm()


### In the following, we read all the devices exported by the channels above
### and then the functions and errcodes exported by those devices.
# devcodes has devname associated with a number, e.g. {'tvc': 1, 'mfc-ch4-1':2, ...}
# funccodes is a list, whose nth item is a dict which maps function names for device 
# number n to integers. Similarly errcodes is a list for each of the devices.
# This is done to reduce the data needed for communication with clients and faster parsing.
devcodes, funccodes, errcodes, devnum = bidict.bidict({}), [], [], 0


################ Virtual Controller.
# device number 0 is always the virtual controller and it is used for
# configuring logging, managing scripts etc. A special task is to provide
# the clients with the mapping devcodes, funccodes, errcodes.
# these functions are defined below.
#### Confiure logging.
import logging
import logging.handlers
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s| %(message)s')
default_format = logging.Formatter('%(asctime)s| %(message)s')
# add default log file location, 20 MB per file and 5 files at most.
LOGDIR = './logs/'
LOG_FILENAME = LOGDIR+'controlserver.log'
handler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=20*1024*1024, backupCount=5)
handler.setFormatter(default_format)
cs_logger = logging.getLogger('')
cs_logger.addHandler(handler)
additional_loggers = []

# this adds a named file to the logger
def start_logfile(args):
    h = logging.FileHandler(LOGDIR+args[0]+'.log', mode='w+')
    h.setFormatter(default_format)
    cs_logger.addHandler(h)
    additional_loggers.append(h)
    cs_logger.info('New run: '+args[1])
    return 0, 'Appending logs to '+args[0]

# removes the last added log file. todo: remove only the named files
def stop_logfile(args): # remove the latest file logger.
    cs_logger.removeHandler(additional_loggers.pop())
    return 0, 'Stopping log to file'

import scriptmanager
def savescript(args):
    return 0, scriptmanager._save_script(args[0])

# get_* functions used by the clients to get the numeric mappings of devs and funcs.
devcodes['controller'] = devnum
def get_allcodemaps(args, devs=devcodes, funcs=funccodes, errs=errcodes):
    m1 = dict(devs)#, dict(bidict.inverted(devs))
    m2 = dict(bidict.inverted(m1)) # dict(bidict.inveted(devs)) doesn't work.
    m3, m4 = [], []
    for f in funcs:
        m3.append(dict(f))
        finv = dict(bidict.inverted(dict(f)))
        m4.append(finv)
    print(m3)
    return 0, [m1, m2, m3, m4, errs]
def get_funcdescript(args): return 0, [[]] # this is for help on all the functions.

SERVER_IPADDR = '127.0.0.1' # this is set from interface server too.
DISALLOWED_IP = []
ALLOWED_IP = [SERVER_IPADDR]
CLIENT_INFO = {}
def authorise(args, check_only=False): 
    # allow all clients at the moment.
    global ALLOWED_IP, DISALLOWED_IP, SERVER_IPADDR, CLIENT_INFO
    ip = CLIENT_INFO['ip']
    if ip in ALLOWED_IP: return 0, ['Authorisation OK. Send commands.']
    elif ip in DISALLOWED_IP: return -1, ['Commands from your IP addr not allowed.']
    elif check_only==True:
        # allow first commands
        return 0, ['First command is allowed.']
    elif check_only==False: # allow all IPs for now.
        ALLOWED_IP += [CLIENT_INFO['ip']]
        return 0, ['Authorisation OK. Send commands.']
    else: return -1, ['Auth not granted.']

controlfuncs = bidict.bidict({'getcodes': get_allcodemaps, 'getfuncdescript': get_funcdescript,\
        'startlog': start_logfile, 'stoplog': stop_logfile, 'savescript': savescript, 'authorise':authorise})
controlerrcodes = {-1: 'Invalid Arguments'}
controlfuncs_list = [controlfuncs[:x] for x in [get_allcodemaps, get_funcdescript, start_logfile, stop_logfile, savescript, authorise]]
### functions of controller end here. todo: pack all of them in a class.


# we now populate functions maps for all the devices. It is a two level dict,
# first key is for device name and second key is for function name. The final
# value is handle to executable function. errmap is not needed since errcodes
# are transmitted only from devices to the clients, so errcodes list is enough.
funcmap = {}
funcmap['controller'] = controlfuncs
# bidicts funccodes, devcodes are used in get_allcodemaps to provide both way mapping.
funccodes += [bidict.bidict(zip(controlfuncs_list, range(len(controlfuncs))))]
errcodes += [controlerrcodes]
for k, v in devconfig.devlist.items():
    devnum += 1
    f, e = chans[v['conn']].funcmap(k, v['devid'])
    funcmap[k] = f
    devcodes[k] = devnum
    funcnames = funcmap[k].keys()
    funccodes += [bidict.bidict(zip(funcnames, range(len(funcnames))))]
    errcodes += [e]
    # now funcmap has the functions available for each of the devices.
print(devcodes)
print(funccodes)

cs_logger.debug("Reading initial config from devconfig.py and initialising the devices.")
for d in devconfig.devlist.keys():
    if 'init_state' in funcmap[d]: 
        cs_logger.debug("Initialising %-20s | init:%s"%(d, str(devconfig.devlist[d]['init_params'])))
        funcmap[d]['init_state'](devconfig.devlist[d]['init_params'])
# generate the index.html (i.e. the UI seen in browser)
#devconfig.gen_html_ui()
cs_logger.debug("Device initialisation finished.")


# Following takes commands coming from clients and executes them on 
# the devices, packs their response towards the clients.
#cmdlist = json.loads(line) or msgpack.unpack(line)
def execute_commands(cmdlist, fmt=0, client_info={'ip':'127.0.0.1'}): # fmt=0 for numeric codes, =1 for string cmds
    global ALLOWED_IP, DISALLOWED_IP, SERVER_IPADDR, CLIENT_INFO
    CLIENT_INFO = client_info # this is set so that controller['authorise'] function can have access to client IP
    auth, authmsg = authorise([], check_only=True)
    if auth<0: return [[0, funccodes[0]['authorise'], [auth, authmsg]]]
    def get_response(resp, func, args): return resp.append(func(args))
    cs_logger.debug(str(cmdlist))
    i, t, resplist, func = 0, [], [], None #[[] for i in range(len(cmdlist))] 
    for c in cmdlist:
        #print(devcodes.inv[cmdlist[0][0]])   print(funccodes[cmdlist[0][0]].inv[cmdlist[0][1]])
        #print(c[0], c[1], c[2])
        resplist.append([c[0], c[1]]) # this adds a new list to resps
        if fmt==0: func = funcmap[devcodes.inv[c[0]]][funccodes[c[0]].inv[c[1]]]
        elif fmt==1: func = funcmap[c[0]][c[1]]
        # resps[i] is the newly added list and is populated by response
        # this way all the responses are in correct sequence. 
        t.append(threading.Thread(target=get_response, args=(resplist[i], func, c[2])))
        i += 1
    [x.start() for x in t]
    [x.join() for x in t]
    # resps now have all the responses in correct sequence.
    cs_logger.debug(str(resplist))
    return resplist

#execute_commands([[0, 0, [1]]]) #, [0, 1, [2, 3, 4]]])

## If the module is run on command line, serve the commands on port 9999.
if __name__ == '__main__':
    import socket
    import threading
    class Handler(socketserver.StreamRequestHandler):
        def __init__(self, request, client_address, server):
            self.client = client_address
            self.socket = request
            self.thread = threading.currentThread()
            cs_logger.debug('Started talking to '+str(self.client[0])+':'+
                    str(self.client[1])+' on '+self.thread.getName())
            socketserver.StreamRequestHandler.__init__(self, request, client_address, server)
            return

        def setup(self): return socketserver.StreamRequestHandler.setup(self)

        def finish(self): return socketserver.StreamRequestHandler.finish(self)

        def handle(self):
            while True:          
                line = self.rfile.readline()
                print(line)
                print(line.decode('utf-8'))
                line = line.decode('utf-8')
                cs_logger.debug(line)
                res = []
                try:
                    d = json.loads(line)
                    print(d)
                    res = execute_commands(d, fmt=0)
                except Exception as e:
                    res.append({'status':'Error', 'error': repr(e)+line})
                finally:
                    cs_logger.debug(json.dumps(res))
                    self.wfile.write(bytes(json.dumps(res)+'\n', "utf-8"))
            return

    class multiServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
        allow_reuse_address = True
        pass

    server = multiServer(('0.0.0.0',9999), Handler)
    print('\nListening on:', server.server_address, ' for requests.')
    server.serve_forever()

